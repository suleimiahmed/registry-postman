# Registry Postman :mailbox:

![alt text](registry-tanuki-gopher.gif "Registry-tanuki-gopher")

A small collection of Postman requests for learning/testing against different container registry (& dependecy proxy) environments. The extensive list of (not yet supported) container registry APIs can be found at:

- [gitlab/v1](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs-gitlab/api.md)
- [/v2](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs/spec/api.md)

and most of the corresponding use-cases/flows for those APIs can be found here:

- [Push/Pull](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs-gitlab/push-pull-request-flow.md)
- [Auth](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs-gitlab/auth-request-flow.md)

## Getting started

1. Have [Postman installed](https://www.postman.com/downloads/).
2. From the postman UI import one or all of the postman json files from the each of the environments folder in the root repo.
3. Run each requests from the postman UI

## Things to know

- Some registry requests are gated by Authourization. In order to run them you must obtain the correct authourization token by running the `auth` request for each environment (in the loaded postman file), which should return a token with the scope you requested. Use this token in each request that requires the appropriate scope by explicitly adding it to the postman request's UI.

- Each postman environment (i.e gdk, local, gstg ...) has the global variable set for `registry-host` and `gittlab-api-host`. For each environment those variables are set as highlighted in the table below

|    `env`    |        `registry-host`       | `gittlab-api-host`  |
| ----------- | -----------------------------| ------------------- |
| `gdk`       | `registry.test:5000`         | `gdk.test:3000`     |
| `gstg`      | `registry.staging.gitlab.com`| `staging.gitlab.com`|
| `gprd`      | `registry.gitlab.com`        | `gitlab.com`        | 
| `local`     | `localhost:5000`             | `-`                 |

feel free to modify these variables from the UI as necessary for your use case.

## Contribution direction :bulb: 

This project is looking for contributors to help with:

- Fleshing out all the API requests that the container registry currently supports into postman `json` formats :meat_on_bone: 

- Build out an [A/B testing](https://www.oracle.com/cx/marketing/what-is-ab-testing/) suite that can deploy two diffrent versions of the container registry (possibly in different environments) and target them with similar postman requests to confirm they respond in a similar manner. The idea behind this is to have a data driven approach (maybe through grafana?) to make lukewarm validations on if one version of the registry has changed in comparison to another, without having to manually setup and tear down each environment.
